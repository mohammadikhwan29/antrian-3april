<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AntrianController;
use App\Http\Controllers\PasienController;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/admin', function () {
//     return view('welcome');
// });


Route::get('/admin', [PasienController::class, 'antrian'])->name('antrian');

Route::get('/', [PasienController::class, 'pengunjung'])->name('pengunjung');

Route::get('/pasien', [AntrianController::class, 'index'])->name('pasien');

Route::get('/tambahpasien', [AntrianController::class, 'tambahpasien'])->name('tambahpasien');
Route::post('/insertdata', [AntrianController::class, 'insertdata'])->name('insertdata');

Route::get('/tampildata/{id}', [AntrianController::class, 'tampildata'])->name('tampildata');
Route::post('/updatedata/{id}', [AntrianController::class, 'updatedata'])->name('updatedata');

Route::get('/delete/{id}', [AntrianController::class, 'delete'])->name('delete');



Route::get('/klik', [PasienController::class, 'klik'])->name('klik');
Route::get('/antrian', [PasienController::class, 'antrian'])->name('antrian');

Route::get('/hapus/{id}', [PasienController::class, 'hapus'])->name('hapus');
Route::get('/ada/{id}', [PasienController::class, 'ada'])->name('ada');
Route::get('/data', [PasienController::class, 'data'])->name('data');

Route::get('/kembali', [PasienController::class, 'kembali'])->name('kembali');

Route::get('/back', [PasienController::class, 'back'])->name('back');
Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');


Route::get('/print/{id}', [PasienController::class, 'print'])->name('print');

Route::get('/tampil/{id}', [PasienController::class, 'tampil'])->name('tampil'); 
