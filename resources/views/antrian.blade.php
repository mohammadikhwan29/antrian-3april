@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    
@stop

@section('content')

    <h1 class="m-0"><center>Data Antrian</center></h1>
    <br>
    <a href="/back" type="button" class="btn btn-warning">Ambil Nomor Antrian</a>
<div class="container">
        <div class="row">
            <table class="table">
                <thead>
                    <br><br><br>
                  <tr>
                    <th scope="col">Nomor Antrian</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>

                @foreach ($data as $row)
                
                <tr>
                <td>{{ $row->id }}</td>
                    <td>
                    
                        <a onClick="togglePlay()"> <button class="btn btn-success" type="button">Panggil</button></a>
                        <audio id="myAudio" src="/suara/{{$row->id}}.mp3"></audio>                          
                        <a href="/ada/{{ $row->id }}" class="btn btn-info">Ada</a>
                        <a href="/hapus/{{ $row->id }}" class="btn btn-danger">Tidak Ada</a>
                        <a href="/print/{{ $row->id }}" target="_blank" class="btn btn-danger">Print</a>
                    </td>
                  </tr>
                  
                @endforeach

                  
                </tbody>
              </table>

            </div>
            </div>
            <script type="text/javascript">
                    var myAudio = document.getElementById("myAudio");
                    var isPlaying = false;

                    function togglePlay() {
                      if (isPlaying) {
                        myAudio.pause()
                      } else {
                        myAudio.play();
                      }
                    };
                    myAudio.onplaying = function() {
                      isPlaying = true;
                    };
                    myAudio.onpause = function() {
                      isPlaying = false;
                    };
                  </script>
        </div>
</div>
</body>
</html>

@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop