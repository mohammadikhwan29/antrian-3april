@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    
@stop

@section('content')

    <h1 class="m-0"><center>Data Pasien</center></h1>
    <div class="container">
        <a href="/tambahpasien" type="button" class="btn btn-success">Tambah Data Pasien</a>
        <div class="row">
          @if ($message = Session::get('success'))
          <div class="alert alert-primary" role="alert">
             {{ $message }}
          </div>
          @endif
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Jenis Kelamin</th>
                    <th scope="col">Umur</th>
                    <th scope="col">No telepon</th>
                    <th scope="col">Poli</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>

                @php
                $no = 1; 
                @endphp

                @foreach ($data as $row)
                <tr>
                    <th scope="row">{{ $no++ }}</th>
                    <td>{{ $row->nama }}</td>
                    <td>{{ $row->jeniskelamin }}</td>
                    <td>{{ $row->umur }} Tahun</td>
                    <td>0{{ $row->notelpon }}</td>
                    <td>{{ $row->poli }}</td>
                    <td>{{ $row->alamat }}</td>
                    <td>
                        <a href="/tampildata/{{ $row->id }}"  class="btn btn-info">Edit</a>
                        <a href="/delete/{{ $row->id }}" class="btn btn-danger">Delete</a>
                    </td>
                  </tr>
                @endforeach

                  
                </tbody>
              </table>
              <a href="/kembali" type="button" class="btn btn-warning">Kembali Ke Antrian</a>
        </div>
      </div>
</div>


@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop