<?php

namespace App\Http\Controllers;

use App\Models\Pasien;
use App\Models\Antrian;
use Illuminate\Http\Request;
use PDF;

class PasienController extends Controller
{
    public function pengunjung(){
        $data = Pasien::all();
        return view('pengunjung', compact('data'));
    }

    public function klik(Request $request){
        Pasien::create($request->all());
         return redirect()->route('pengunjung');
    }

    public function antrian(){
        $data = Pasien::all();
        return view('antrian', compact('data'));
    }

    public function hapus($id){
        $data = Pasien::find($id);
        $data->delete();
        return redirect()->route('antrian');
    }

    public function data(){
        $data = Antrian::all();
        return view('datapasien', compact('data'));
    }

    public function ada($id){
        $data = Pasien::find($id);
        $data->delete();
        return redirect()->route('data');
    }

    public function kembali(){
        $data = Pasien::all();
        return view('antrian', compact('data'));
    }

    public function back(){
        return redirect()->route('pengunjung');
    }

    public function print($id){
        $data = Pasien::find($id);
        return view('print', compact('data'));
    }
}
